#ifndef _TMITEMPLATEFOLDER_H_
#define _TMITEMPLATEFOLDER_H_

//standard libraries
#include <string>
#include <sstream>

//my namespace
namespace tmi {

	//template for number conversion to string
	template<typename T>
	std::string NumberToString(T parameter) {	
		std::stringstream out;  
		out << parameter;  
		return out.str();
	}



}

#endif