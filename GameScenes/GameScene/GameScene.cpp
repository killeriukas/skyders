#include "GameScene.h"
#include <sfml/Graphics.hpp>
#include "Piece.h"
#include "Board.h"
#include "../../InputSystem/InputHandler.h"
#include "../../collisiondetectionclass.h"
#include "../../TextSystem/Text.h"
#include "../../tmitemplatefolder.h"
#include "../../Util/HelpFunctions.h"

GameScene::GameScene(sf::RenderWindow* renderWindow, float winWidth, float winHeight) : AScene("GameScene") {
	mg_RenderWindowClone = renderWindow;
	_winWidth = winWidth;
	_winHeight = winHeight;
	m_Board = 0;
	m_Piece = 0;
	m_Text = 0;
}

GameScene::~GameScene() {

}

void GameScene::Shutdown() {
	//deletes board object
	SAFE_DELETE(m_Board);

	//deletes piece object
	SAFE_DELETE(m_Piece);

	//deletes text object
	SAFE_ARRAY_DELETE(m_Text);
}

//abstract play functions
void GameScene::Start() {
	bool result;

	//if given pointer is NULL, return ERROR
	if(!mg_RenderWindowClone) {
		return;
	}

	//creates board object
	m_Board = new Board;

	if(!m_Board) {
		return;
	}

	//initializes board object
	result = m_Board->Initialize(_winWidth, _winHeight);

	if(!result) {
		return;
	}

	//creates piece object
	m_Piece = new Piece(m_Board->GetCubeSize());

	if(!m_Piece) {
		return;
	}

	//initializes piece object
	result = m_Piece->Initialize(m_Board->GetLeftSideX(), m_Board->GetRightSideX(), m_Board->GetTopY());

	if(!result) {
		return;
	}

	//allocate memory for text object
	m_Text = new Text[3];

	if(!m_Text) {
		return;
	}

	//initialize text object
	result = m_Text[0].Initialize(5.0f, 5.0f);

	if(!result) {
		return;
	}

	//initialize text object
	result = m_Text[1].Initialize(_winWidth - 110.0f,5.0f);

	if(!result) {
		return;
	}

	//initialize text object
	result = m_Text[2].Initialize(_winWidth * 0.5f - 50.0f,(_winHeight * 0.5f));

	if(!result) {
		return;
	}

	//set text
	m_Text[1].SetSentence("Next Piece");
	
	//set text for game over
	m_Text[2].SetSentence("");
	m_Text[2].SetTextColor(0,0,0);

}


void GameScene::Stop() {

	//delete the objects information
	Shutdown();
}

void GameScene::Resume() {}
void GameScene::Pause() {}

void GameScene::UpdateInput(float deltaTimeMS) {

	bool keydown;

	//if game was over, you return true and don't handle keyboard input
	if(m_Board->IsGameOver()) return;

	//set delta time for piece movement
	m_Piece->SetFrameTime(deltaTimeMS);

	//check buttons
	keydown = m_InputHandler->IsLeft();
	m_Piece->MovePieceLeft(keydown);

	keydown = m_InputHandler->IsRight();
	m_Piece->MovePieceRight(keydown);

	keydown = m_InputHandler->IsRotate();
	m_Piece->RotatePiece(keydown);

	keydown = m_InputHandler->IsDropInstant();
	m_Piece->MovePieceDownInstant(keydown);

}

void GameScene::InstantDown(bool clicked) {
		
	bool result;

	if(clicked) {

		result = false;

		while(!result) {
			//drop a piece for one unit
			m_Piece->DropPiece();

			//check board and piece collision
			//returns true if collision detected, and false - otherwise
			//this collision saves your piece into the board and spawn a new one
			result = CollisionDetectionClass::isCollisionDetected(m_Board, m_Piece);

			//if collision detected, return to previous good state
			//save piece to the board and spawn the second one
			if(result) {
				m_Piece->ResetState();
				m_Board->SavePieceToBoard(m_Piece);
				m_Piece->SpawnNextPiece();
			} 

			//save current good state of piece object
			m_Piece->SaveGoodState();
		}
	}
}

void GameScene::UpdateGameLogic(float deltaTimeMS) {

	bool result;

	//if game has been lost, stop all game logics
	if(!m_Board->IsGameOver()) {
		
		//check instant drop feature
		InstantDown(m_Piece->GetFastDropState());

		//set score in piece object, to affect the dropping speed
		m_Piece->SetScore(m_Board->GetScore());

		//animate the board in case you need it
		m_Board->Animate(deltaTimeMS);

		//this collision just doesn't let you move
		result = CollisionDetectionClass::isMoveCollision(m_Board, m_Piece);

		if(result) {
			m_Piece->ResetState();
		}

		//save new state of dropping piece
		m_Piece->SaveGoodState();

		//drop a piece for one unit
		m_Piece->DropPiece();

		//check board and piece collision
		//returns true if collision detected, and false - otherwise
		//this collision saves your piece into the board and spawn a new one
		result = CollisionDetectionClass::isCollisionDetected(m_Board, m_Piece);

		//if collision detected, return to previous good state
		//save piece to the board and spawn the second one
		if(result) {
			m_Piece->ResetState();
			m_Board->SavePieceToBoard(m_Piece);
			m_Piece->SpawnNextPiece();
		} 


		//save current good state of piece object
		m_Piece->SaveGoodState();

		//get current score from board object
		std::string currentScore = "Score: " + tmi::NumberToString(m_Board->GetScore());

		//set current score in the text object
		m_Text[0].SetSentence(currentScore);
	
	} else {

		static float timeLeft = 3.0f;
		m_Text[2].SetSentence("Game Over!");

		//decrement the time
		timeLeft -= deltaTimeMS;

		//lets you understand, that you have lost and go back to menu screen
		if(timeLeft < 0) {
			timeLeft = 3.0f;
			ChangeScene("MenuScene");
		}

	}
}

void GameScene::Render() {

	//clears buffer
	mg_RenderWindowClone->clear();

	//render current board
	m_Board->Render(mg_RenderWindowClone);

	//render pieces
	m_Piece->Render(mg_RenderWindowClone);

	//render text
	m_Text[0].Render(mg_RenderWindowClone);
	m_Text[1].Render(mg_RenderWindowClone);
	m_Text[2].Render(mg_RenderWindowClone);

	//swap buffers
	mg_RenderWindowClone->display();
}