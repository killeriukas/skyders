#include "Piece.h"
#include <sfml/Graphics.hpp>


Piece::Piece(const float pCUBE_SIZE) : CUBE_SIZE(pCUBE_SIZE) {
	m_cube = 0;
	m_defaultSpeed = 1.0f;
	m_fastDrop = false;
}

Piece::Piece(const Piece& other, const float pCUBE_SIZE) : CUBE_SIZE(pCUBE_SIZE) {

}

Piece::~Piece() {
	delete m_cube;
	m_cube = 0;
}

bool Piece::Initialize(float minX, float maxX, float topY) {

	/* initialize random seed: */
	srand ( (unsigned int) time(NULL) );

	//set left board side, right side, and top
	m_minX = minX;
	m_maxX = maxX;
	m_topY = topY;

	m_cube = new sf::RectangleShape();

	//set initial cube parameters, i.e. cube size and color
	m_cube->setSize(sf::Vector2f(CUBE_SIZE, CUBE_SIZE));
	m_cube->setFillColor(sf::Color(255,255,255));

	//set next piece
	SetRandomPiece(m_nextPieceType, m_nextRotation);

	//set next piece the current one, and spawn another one
	SpawnNextPiece();

	return true;
}

void Piece::SpawnNextPiece() {

	//set coordinates for a next piece
	m_nextPositionX = m_maxX + CUBE_SIZE;

	//move y coordinate per 4 cube_sizes
	//and it is drawn from (0,0) point, i.e. top left corner
	m_nextPositionY = m_topY - 3*CUBE_SIZE;

	//make next piece a current one
	m_PieceType = m_nextPieceType;

	//make next piece's rotation a current one
	m_rotation = m_nextRotation;

	//set current piece position
	//m_positionX = m_minX + (m_maxX - m_minX - 4 * CUBE_SIZE) * ((float)(rand() % 100) * 0.01f);
	m_positionX = (m_minX + m_maxX) * 0.5f - 2 * CUBE_SIZE;

	//move y coordinate per 3 cube_sizes
	//because cube needs to be above board
	//and it is drawn from (0,0) point, i.e. top left corner
	m_positionY = m_topY - 3 * CUBE_SIZE;
	
	//set last good position for piece position
	SaveGoodState();

	//get the next piece after this
	SetRandomPiece(m_nextPieceType, m_nextRotation);

}

void Piece::SetRandomPiece(PieceType& piece, int& rotation) {

	//get random number between 0 - 6
	unsigned int randomNr = rand() % 7;

	//get random rotation for a piece
	rotation = rand() % 4;

	//set the piece property to random figure
	switch(randomNr) {
		case 0: piece = PIECE_CUBE; break;
		case 1: piece = PIECE_L; break;
		case 2: piece = PIECE_I; break;
		case 3: piece = PIECE_ML; break;
		case 4: piece = PIECE_T; break;
		case 5: piece = PIECE_S; break;
		case 6: piece = PIECE_MS; break;
	}
}

void Piece::SaveGoodState() {
	//set last good position for piece position
	m_lastGoodX = m_positionX;
	m_lastGoodY = m_positionY;

	//set last good rotation for a piece
	m_lastGoodRot = m_rotation;
}

void Piece::ResetState() {
	//set the last good known coordinates
	m_positionX = m_lastGoodX;
	m_positionY = m_lastGoodY;
	m_rotation = m_lastGoodRot;
}

void Piece::Render(sf::RenderWindow* renderWindow) {

	//draws a current piece
	DrawDroppingPiece(renderWindow);

	//draw next piece
	DrawNextPiece(renderWindow);

}

void Piece::DrawDroppingPiece(sf::RenderWindow* renderWindow) {

	//show dropping piece 
	float x,y;

	//set dropping piece coordinates
	x = m_positionX;
	y = m_positionY;

	//start drawing of cubes in table
	for(int width = 0; width < 4; ++width) {
		for(int height = 0; height < 4; ++height) {

			//moves cube forward
			m_cube->setPosition(x+width*CUBE_SIZE,y+height*CUBE_SIZE);

			//draws the cubes, which are filled ( == 1)
			if(PIECE_COLLETION[m_PieceType][m_rotation][width][height] == 1) {
				renderWindow->draw(*m_cube);
			}
		}
	}

}

void Piece::DrawNextPiece(sf::RenderWindow* renderWindow) {

	//show next figure in this place
	float x, y;

	//set next piece coordinates
	x = m_nextPositionX;
	y = m_nextPositionY;

	//start drawing of cubes in table
	for(int width = 0; width < 4; ++width) {
		for(int height = 0; height < 4; ++height) {

			//moves cube forward
			m_cube->setPosition(x+width*CUBE_SIZE,y+height*CUBE_SIZE);

			//draws the cubes, which are filled ( == 1)
			if(PIECE_COLLETION[m_nextPieceType][m_nextRotation][width][height] == 1) {
				renderWindow->draw(*m_cube);
			}
		}
	}
}

void Piece::SetFrameTime(float deltaT) {
	m_deltaTime = deltaT;
}

void Piece::MovePieceLeft(bool clicked) {

	//let player push a button only once per 1/5 second
	//if you don't have this limitation, piece gonna fly, instead of moving
	static float pushedOnce = 0.2f;
	
	pushedOnce -= m_deltaTime;
	if((pushedOnce < 0) && clicked) {
		m_piecePhase = MOVE;
		m_positionX -= CUBE_SIZE;
		pushedOnce = 0.2f;
	}

}

void Piece::MovePieceRight(bool clicked) {

	//let player push a button only once per 1/5 second
	//if you don't have this limitation, piece gonna fly, instead of moving
	static float pushedOnce = 0.2f;

	pushedOnce -= m_deltaTime;
	if((pushedOnce < 0) && clicked) {
		m_piecePhase = MOVE;
		m_positionX += CUBE_SIZE;
		pushedOnce = 0.2f;
	}
}

void Piece::RotatePiece(bool clicked) {

	//let player push a button only once per 1/5 second
	//if you don't have this limitation, piece gonna fly, instead of moving
	static float pushedOnce = 0.2f;

	pushedOnce -= m_deltaTime;
	if((pushedOnce < 0) && clicked) {
		m_piecePhase = MOVE;
		m_rotation++;
		m_rotation %= 4;
		pushedOnce = 0.2f;
	}
}

int Piece::GetPiece(int width, int height) const {
	return PIECE_COLLETION[m_PieceType][m_rotation][width][height];
}

float Piece::GetCurrentPositionX() const {
	return m_positionX;
}

float Piece::GetCurrentPositionY() const {
	return m_positionY;
}

void Piece::SetScore(int score) {
	m_score = score;
}

void Piece::DecreaseTimeBetweenDrop() {

	static int decrementTimes = 0;

	//if speed is the fastest one, leave it as it is, don't make it to teleport
	if(m_defaultSpeed < 0.05f)  { 
		m_defaultSpeed = 0.04f;
	} else {
		//every each hundred of score, the speed increases
		if((int) (m_score / 100) > decrementTimes) {
			++decrementTimes;
			m_defaultSpeed -= 0.1f;	
		}
	}
}

void Piece::DropPiece() {

	//set the current dropping speed to the default one
	static float droppedOnce = m_defaultSpeed;

	//drop cube once per default speed
	droppedOnce -= m_deltaTime;
	if(droppedOnce < 0) {
		m_piecePhase = DROP;
		droppedOnce = m_defaultSpeed;
		m_positionY += CUBE_SIZE;

		//check whether we need to increase speed
		DecreaseTimeBetweenDrop();
	}
}

void Piece::MovePieceDownInstant(bool clicked) {
	
	//let player push a button only once per 1/5 second
	//if you don't have this limitation, piece gonna fly, instead of moving
	static float pushedOnce = 0.2f;
	pushedOnce -= m_deltaTime;

	//if you want to drop piece instantly, just set m_fastDrop variable to true
	if((pushedOnce < 0) && clicked) {
		m_piecePhase = DROP;
		m_fastDrop = true;
		pushedOnce = 0.2f;
	}

}

bool Piece::GetFastDropState() const {

	if(m_fastDrop) {
		m_fastDrop = false;
		return true;
	}

	return false;
}

Piece::PiecePhase Piece::GetPiecePhase() const {
	return m_piecePhase;
}