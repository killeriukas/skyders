#include "Board.h"
#include <sfml/Graphics.hpp>
#include <sfml/System/Vector2.hpp>
#include "Piece.h"

Board::Board() : CUBE_SIZE(20.0f) {
	m_cube = 0;
}

Board::Board(const Board& other) : CUBE_SIZE(20.0f) {

}

Board::~Board() {
	delete m_cube;
	m_cube = 0;
}

bool Board::Initialize(float winWidth, float winHeight) {
	
	//center of the board in X direction
	m_initialPixelX = winWidth / 2.0f;

	//top of the board in Y direction
	m_initialPixelY = winHeight - ((float)BOARD_HEIGHT)*CUBE_SIZE;

	//set current score to zero
	m_score = 0;

	//sets table to empty state
	ResetTable();

	m_cube = new sf::RectangleShape();

	//set outline thickness to none
	m_cube->setOutlineThickness(0);

	return true;
}

float Board::GetLeftSideX() const {
	//because initial x is in the middle, we need to subtract only half of x to get left side
	return m_initialPixelX - ((float)BOARD_WIDTH)*CUBE_SIZE*0.5f;
}

float Board::GetRightSideX() const {
	//because initial x is in the middle, we need to add only half of x to get right side
	return m_initialPixelX + ((float)BOARD_WIDTH)*CUBE_SIZE*0.5f;
}

float Board::GetBottomY() const {
	//to get a bottom Y, we need to add full height of a table
	//because initial y is in the top of the table
	return m_initialPixelY + ((float)BOARD_HEIGHT)*CUBE_SIZE;
}

float Board::GetCubeSize() const {
	return CUBE_SIZE;
}

float Board::GetTopY() const {
	return m_initialPixelY;
}

void Board::ResetTable() {
	//empties the table
	for(int width = 0; width < BOARD_WIDTH; ++width) {
		for(int height = 0; height < BOARD_HEIGHT; ++height) {
			BOARD[width][height] = 0;
		}
	}
}

void Board::Render(sf::RenderWindow* const renderWindow) {
	//render table in current state
	DrawTable(renderWindow);
}

void Board::DrawTable(sf::RenderWindow* const renderWindow) {

	//start position of board
	float x, y;

	x = m_initialPixelX - ((float)BOARD_WIDTH * 0.5f)*CUBE_SIZE;
	y = m_initialPixelY;
	
	//set board size and color
	m_cube->setSize(sf::Vector2f(BOARD_WIDTH*CUBE_SIZE, BOARD_HEIGHT*CUBE_SIZE));
	m_cube->setFillColor(sf::Color(125,125,125));

	//moves board forward
	m_cube->setPosition(x,y);

	//draw a board
	renderWindow->draw(*m_cube);

	//set cube size and color
	m_cube->setSize(sf::Vector2f(CUBE_SIZE, CUBE_SIZE));
	m_cube->setFillColor(sf::Color(255,255,255));

	//start drawing of cubes in table
	for(int width = 0; width < BOARD_WIDTH; ++width) {
		for(int height = 0; height < BOARD_HEIGHT; ++height) {
		
			//moves cube forward
			m_cube->setPosition(x+width*CUBE_SIZE,y+height*CUBE_SIZE);

			//draws the cubes, which are filled ( == 1)
			if(BOARD[width][height] == 1) {
				renderWindow->draw(*m_cube);
			}
		}
	}
}

void Board::SavePieceToBoard(const Piece* const piece) {

	//var for changing coordinates from piece world into board world
	int pieceInBoardX, pieceInBoardY;

	for(int width = 0; width < 4; ++width) {
		for(int height = 0; height < 4; ++height) {

			//check which parts of piece cube are active
			if(piece->GetPiece(width, height) == 1) {

				//count piece coordinates in board array coordinates
				//NOT PIXELS!!!
				pieceInBoardX = (int)((piece->GetCurrentPositionX() - (m_initialPixelX - ((float)BOARD_WIDTH * 0.5f)*CUBE_SIZE)) / CUBE_SIZE) + width;
				pieceInBoardY = (int)((piece->GetCurrentPositionY() - m_initialPixelY) / CUBE_SIZE ) + height;

				//activate the slot in the board
				BOARD[pieceInBoardX][pieceInBoardY] = 1;
			}
		}
	}

	//check whether we have a full active row
	CheckBoardRow();
}

int Board::GetBoardTileState(float x, float y) const {

	//count piece coordinates in board array coordinates
	//NOT PIXELS!!!
	int pieceInBoardX = (int)((x - (m_initialPixelX - ((float)BOARD_WIDTH * 0.5f)*CUBE_SIZE)) / CUBE_SIZE);
	int pieceInBoardY = (int)((y - m_initialPixelY) / CUBE_SIZE );

	//return 0, in case coordinates are out of bounds of board
	//when piece spawns, it is above board, so it crashes the program
	if(pieceInBoardX >= BOARD_WIDTH || pieceInBoardX < 0) { return 0; }
	if(pieceInBoardY >= BOARD_HEIGHT || pieceInBoardY < 0) { return 0; }

	//return state of board tile, i.e. 0 as empty, 1 as full
	return BOARD[pieceInBoardX][pieceInBoardY];
}

void Board::CheckBoardRow() {

	int count, height, combo, temporaryScore;

	//set height into the most bottom line
	height = BOARD_HEIGHT-1;

	//score modifier
	combo = 0;

	//set temporary score to zero
	temporaryScore = 0;

	//start from the bottom
	while(height >= 0) {

		//set counter to zero
		count = 0;

		for(int width = 0; width < BOARD_WIDTH; ++width) {

			//check whether this cell is active
			if(BOARD[width][height] == 1) {

				//increment counter if you have found an active cell
				++count;
			}
		}	//first loop ends here

		//if we found a full row do following
		if(count == BOARD_WIDTH) {

			//adds the current full row into the queue of deleted rows, so it could be animated
			AddToDeleteQueue(height);
			
			//you get one combo plus per one line
			++combo;
			
			//you get 10 points per one line
			//plus every each new line gives you additional 10 points
			temporaryScore += 10 * combo;

		}

		//move to the next line
		--height;
	}

	//add new score with your current score
	m_score += temporaryScore;
}

void Board::DeleteRowAndPushBoard(int pHeight) {

	//deletes the full row
	for(int width = 0; width < BOARD_WIDTH; ++width) {
		BOARD[width][pHeight] = 0;
	}

	//if this row is the top one - return
	if(pHeight == 0) { return; }

	//if not the top one, push all above lines down per one line
	for(int height = pHeight-1; height >= 0; --height) {
		for(int width = 0; width < BOARD_WIDTH; ++width) {
			
			//moves above line down per one line
			BOARD[width][height+1] = BOARD[width][height];

		}
	}
}

void Board::AddToDeleteQueue(int height) {
	m_DeleteRowQueue.push_back(height);
}

void Board::Animate(float deltaTimeMS) {

	//if there are no rows waiting for deletion, just skip this function
	if(m_DeleteRowQueue.size() == 0) { return; }

	//set blink time for 1/4 second
	static float blinkTime = 0.25f;

	//counter for deletion
	static int counter = 0;

	//where 1 - visible, 0 - invisible
	int setCubeVisibility = 1;

	//decrement blink time
	blinkTime -= deltaTimeMS;

	//every second time change cube visibility settings
	//first time make the row invisible, second visible and so on...
	if(counter % 2 == 0) { setCubeVisibility = 0; } else { setCubeVisibility = 1; }
	
	if(blinkTime < 0) {
		blinkTime = 0.25f;
		++counter;

		//go through all the elements in the delete queue
		for(int i = 0, len = m_DeleteRowQueue.size(); i < len; ++i) {
			for(int width = 0; width < BOARD_WIDTH; ++width) {

				//change the visibility to achieve blinking effect
				BOARD[width][m_DeleteRowQueue.at(i)] = setCubeVisibility;
			}
		}
	}

	//if cubes have blinked 4 times already
	//count from 0, so when you get counter equal 3, cube has already blinked 4 times
	if(counter == 3) {
		//go through all the elements in the delete queue
		for(int i = 0, len = m_DeleteRowQueue.size(); i < len; ++i) {
			DeleteRowAndPushBoard(m_DeleteRowQueue.at(i) + i);		
		}
		m_DeleteRowQueue.clear();
		counter = 0;
	}
}

bool Board::IsGameOver() const {

	//go through the top line of the board
	for(int width = 0; width < BOARD_WIDTH; ++width) {

		//if any spot is filled in the top line, you've failed
		if(BOARD[width][0] == 1) {
			//game over
			return true;
		}
	}

	//there was no spots filled, so you can continue the game
	return false;
}


int Board::GetScore() const {
	return m_score;
}