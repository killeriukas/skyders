#ifndef _GAMESCENE_H_
#define _GAMESCENE_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a game play scene. It is responsible for the whole game play. It holds pieces,
main board, scores and everything what is included in the game.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//fast forward declarations
namespace sf {
	class RenderWindow;
};
class Piece;
class Board;
class Text;

//for inheritance
#include "../../Scenes/AScene.h"

//game scene class was inherited from scene interface class
class GameScene : public AScene {
public:
	GameScene(sf::RenderWindow*, float, float);
	virtual ~GameScene();

	//abstract play functions
	virtual void Start();
	virtual void Stop();
	virtual void Resume();
	virtual void Pause();

	//abstract game functions
	virtual void Render();
	virtual void UpdateInput(float);
	virtual void UpdateGameLogic(float);
private:
	void Shutdown();

	//make piece go instantly down to the board
	void InstantDown(bool);

private:

	//classes for a game
	Board* m_Board;
	Piece* m_Piece;
	Text* m_Text;

	float _winHeight, _winWidth;

	//pointer to render window
	//it doesn't contain value, just points to render window
	//whenever the game ends, it just stop pointing to it, and points to 0 instead
	sf::RenderWindow* mg_RenderWindowClone;
};

#endif