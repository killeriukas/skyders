#ifndef _PIECE_H_
#define _PIECE_H_

//IMPLEMENTATION OF ALL POSSIBLE POSITIONS
//[7] - seven parts, [4] (0, 90, 180, 270) - four rotation, [4][4] for the grid of part
//it can be easily improved by adding new pieces to this array, rotations, etc...
static const int PIECE_COLLETION[7][4][4][4] =
{
	//square = 0
	{
		{
			{0,0,0,0},
			{0,1,1,0},
			{0,1,1,0},
			{0,0,0,0}
		},
		{
			{0,0,0,0},
			{0,1,1,0},
			{0,1,1,0},
			{0,0,0,0}
		},
		{
			{0,0,0,0},
			{0,1,1,0},
			{0,1,1,0},
			{0,0,0,0}
		},
		{
			{0,0,0,0},
			{0,1,1,0},
			{0,1,1,0},
			{0,0,0,0}
		},
	},
	//L = 1
	{
		{
			{0,1,0,0},
			{0,1,0,0},
			{0,1,1,0},
			{0,0,0,0}
		},
		{
			{0,0,0,0},
			{0,1,1,1},
			{0,1,0,0},
			{0,0,0,0}
		},
		{
			{0,0,0,0},
			{0,1,1,0},
			{0,0,1,0},
			{0,0,1,0}
		},
		{
			{0,0,0,0},
			{0,0,1,0},
			{1,1,1,0},
			{0,0,0,0}
		}
		},
			//I = 2
		{
			{
				{0,0,1,0},
				{0,0,1,0},
				{0,0,1,0},
				{0,0,1,0}
			},
			{
				{0,0,0,0},
				{0,0,0,0},
				{1,1,1,1},
				{0,0,0,0}
			},
			{
				{0,1,0,0},
				{0,1,0,0},
				{0,1,0,0},
				{0,1,0,0}
			},
			{
				{0,0,0,0},
				{1,1,1,1},
				{0,0,0,0},
				{0,0,0,0}
			}
		},
			//L-Mirrored = 3
		{
			{
				{0,0,1,0},
				{0,0,1,0},
				{0,1,1,0},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,1,0,0},
				{0,1,1,1},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,1,1,0},
				{0,1,0,0},
				{0,1,0,0}
			},
			{
				{0,0,0,0},
				{1,1,1,0},
				{0,0,1,0},
				{0,0,0,0}
			}
		},
			//T = 4
		{
			{
				{0,0,0,0},
				{0,0,1,0},
				{0,1,1,1},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,1,0,0},
				{0,1,1,0},
				{0,1,0,0}
			},
			{
				{0,0,0,0},
				{1,1,1,0},
				{0,1,0,0},
				{0,0,0,0}
			},
			{
				{0,0,1,0},
				{0,1,1,0},
				{0,0,1,0},
				{0,0,0,0}
			}
		},
			//S = 5
		{
			{
				{0,0,1,0},
				{0,1,1,0},
				{0,1,0,0},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,1,1,0},
				{0,0,1,1},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,0,1,0},
				{0,1,1,0},
				{0,1,0,0}
			},
			{
				{0,0,0,0},
				{1,1,0,0},
				{0,1,1,0},
				{0,0,0,0}
			}
		},
			//S-Mirrored = 6
		{
			{
				{0,1,0,0},
				{0,1,1,0},
				{0,0,1,0},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,0,1,1},
				{0,1,1,0},
				{0,0,0,0}
			},
			{
				{0,0,0,0},
				{0,1,0,0},
				{0,1,1,0},
				{0,0,1,0}
			},
			{
				{0,0,0,0},
				{0,1,1,0},
				{1,1,0,0},
				{0,0,0,0}
			}
		}
};

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a piece class. It is responsible for the dropping piece inside the board and for
all the possible actions of the piece.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//forward declarations
namespace sf {
	class RenderWindow;
	class RectangleShape;
};

class Piece {
private:
	enum PieceType {
		PIECE_CUBE = 0,
		PIECE_L = 1,
		PIECE_I = 2,
		PIECE_ML = 3,
		PIECE_T = 4,
		PIECE_S = 5,
		PIECE_MS = 6
	};
	enum PiecePhase {
		MOVE = 0,
		DROP = 1
	};
public:
	Piece(const float pCUBE_SIZE);
	~Piece();
	bool Initialize(float, float, float);
	void Render(sf::RenderWindow*);
	void SetFrameTime(float);
	void SetScore(int);
	void MovePieceLeft(bool);
	void MovePieceRight(bool);
	void MovePieceDownInstant(bool);
	void RotatePiece(bool);
	int GetPiece(int,int) const;
	float GetCurrentPositionX() const;
	float GetCurrentPositionY() const;
	void DropPiece();
	void ResetState();
	void SaveGoodState();
	void SpawnNextPiece();
	PiecePhase GetPiecePhase() const;
	bool GetFastDropState() const;

private:
	Piece(const Piece&, const float pCUBE_SIZE);	
	void DrawDroppingPiece(sf::RenderWindow*);
	void DrawNextPiece(sf::RenderWindow*);
	void SetRandomPiece(PieceType&, int&);
	void DecreaseTimeBetweenDrop();

private:

	//rotation and position of current piece
	float m_positionX, m_positionY, m_lastGoodX, m_lastGoodY;
	int m_rotation, m_lastGoodRot;
	
	//next piece position and rotation
	float m_nextPositionX, m_nextPositionY;
	int m_nextRotation;

	//left side of board, right side of board, top side of board
	float m_minX, m_maxX, m_topY;

	//one cube size
	const float CUBE_SIZE;

	//delta time
	float m_deltaTime;

	//piece phase, whether it dropped or it was moved
	PiecePhase m_piecePhase;

	//piece types
	PieceType m_PieceType;
	PieceType m_nextPieceType;

	//cube settings
	sf::RectangleShape* m_cube;

	//default dropping speed
	//it will decrease each time you get 100 points more
	float m_defaultSpeed;

	//current score
	int m_score;

	//current fast dropping state
	mutable bool m_fastDrop;
};

#endif