#ifndef _BOARD_H_
#define _BOARD_H_

/*the graph of the RenderWindow coordinates in SFML
  -----------------------------------> (x)
  |
  |
  |
  |
  |
  V (y)
*/

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a board class. It is responsible for the board stuff, like drawing, adding
dropping figures and stuff like that.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//impossible to forward declare
#include <vector>

//board height and width
static const int BOARD_WIDTH = 10;
static const int BOARD_HEIGHT = 20;

//current board
static int BOARD[BOARD_WIDTH][BOARD_HEIGHT];

//forward declarations
namespace sf {
	class RenderWindow;
	class RectangleShape;
};
class Piece;

class Board {
public:
	Board();
	Board(const Board&);
	~Board();

	bool Initialize(float, float);
	void Render(sf::RenderWindow* const);
	float GetLeftSideX() const;
	float GetRightSideX() const;
	float GetCubeSize() const;
	float GetTopY() const;
	float GetBottomY() const;
	void SavePieceToBoard(const Piece* const);
	int GetBoardTileState(float, float) const;
	int GetScore() const;
	void Animate(float);
	bool IsGameOver() const;
private:
	
	void DrawTable(sf::RenderWindow* const);
	void ResetTable();
	void CheckBoardRow();
	void DeleteRowAndPushBoard(int);
	void AddToDeleteQueue(int);

private:

	sf::RectangleShape* m_cube;

	//center of the board in X direction
	float m_initialPixelX;

	//top of the board in Y direction
	float m_initialPixelY;

	//cube width, i.e. size
	const float CUBE_SIZE;

	//score amount
	int m_score;

	//queue of waiting row, which need to be deleted
	std::vector<int> m_DeleteRowQueue;
};


#endif