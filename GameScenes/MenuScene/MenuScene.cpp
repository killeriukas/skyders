#include "MenuScene.h"
#include <sfml/Graphics.hpp>
#include "../../Util/HelpFunctions.h"
#include "../../InputSystem/InputHandler.h"
#include "SimpleGUI/GUIManager.h"

MenuScene::MenuScene(sf::RenderWindow* renderWidnow) : AScene("MenuScene") {
	mg_renderWindow = renderWidnow;
	m_texture = 0;
	m_sprite = 0;
	m_GUIManager = 0;
}

MenuScene::~MenuScene() {
	Stop();
}

//abstract play functions
void MenuScene::Start() {

	m_texture = new sf::Texture();

	//load texture
	m_texture->loadFromFile("data/textures/mainbgr.jpg");

	m_sprite = new sf::Sprite(*m_texture);

	m_GUIManager = new GUIManager();
}

void MenuScene::Stop() {
	SAFE_DELETE(m_texture);
	SAFE_DELETE(m_sprite);
	SAFE_DELETE(m_GUIManager);
}

void MenuScene::Resume() {
	//mainly for resuming the pause or something similar
}

void MenuScene::Pause() {
	//mainly for pausing the game for pause menu or something similar
}


void MenuScene::Render() {

	//clears buffer
	mg_renderWindow->clear();

	//draw background
	mg_renderWindow->draw(*m_sprite);

	//render text
	m_GUIManager->Render(mg_renderWindow);

	//swap buffers
	mg_renderWindow->display();
}

void MenuScene::UpdateInput(float deltaTimeMS) {

	if(m_InputHandler->IsEnter()) {
		if(m_GUIManager->GetState() == GUIManager::START_GAME) {
			ChangeScene("GameScene");
		}
		if(m_GUIManager->GetState() == GUIManager::EXIT) {
			ExitApplication();
		}
	}

	//if button up
	if(m_InputHandler->IsRotate()) {
		m_GUIManager->ChangeState(true);
	}

	//if button down
	if(m_InputHandler->IsDropInstant()) {
		m_GUIManager->ChangeState(false);
	}

}

void MenuScene::UpdateGameLogic(float deltaTimeMS) {

}
