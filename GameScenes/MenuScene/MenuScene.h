#ifndef _MENUSCENE_H_
#define _MENUSCENE_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a menu scene. It is responsible for the menu of the start game.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//impossible to fast forward
#include <string>
#include <map>

//for inheritance
#include "../../Scenes/AScene.h"

//fast forward
namespace sf {
	class RenderWindow;
	class Texture;
	class Sprite;
};
class GUIManager;


class MenuScene : public AScene {
public:
	MenuScene(sf::RenderWindow*);
	virtual ~MenuScene();

	//abstract play functions
	virtual void Start();
	virtual void Stop();
	virtual void Resume();
	virtual void Pause();

	//abstract game functions
	virtual void Render();
	virtual void UpdateInput(float);
	virtual void UpdateGameLogic(float);

private:

	GUIManager* m_GUIManager;

	sf::RenderWindow* mg_renderWindow;

	//// Load a sprite to display
	////it's background for main menu
	sf::Texture* m_texture;
	sf::Sprite* m_sprite;
};

#endif