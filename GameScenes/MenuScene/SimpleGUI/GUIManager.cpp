#include "GUIManager.h"
#include "../../../TextSystem/Text.h"
#include "../../../Util/HelpFunctions.h"

GUIManager::GUIManager() {
	m_currentState = START_GAME;
	Initialize();
}

void GUIManager::Initialize() {
	m_Text = new Text[2];
	m_Text[0].Initialize(150,170);
	m_Text[0].SetTextColor(0,0,0);
	m_Text[0].SetSentence("Start Game");
	m_Text[1].Initialize(130,200);
	m_Text[1].SetTextColor(255,255,255);
	m_Text[1].SetSentence("Exit Application");
}

GUIManager::~GUIManager() {
	SAFE_ARRAY_DELETE(m_Text);
}

void GUIManager::Render(sf::RenderWindow* renderWindow) const {
	m_Text[0].Render(renderWindow);
	m_Text[1].Render(renderWindow);
}

void GUIManager::ChangeState(bool moveUp) {
	if(moveUp) {
		m_Text[0].SetTextColor(0,0,0);
		m_Text[1].SetTextColor(255,255,255);
		m_currentState = START_GAME;
	} else {
		m_Text[1].SetTextColor(0,0,0);
		m_Text[0].SetTextColor(255,255,255);
		m_currentState = EXIT;
	}
}

GUIManager::ButtonState GUIManager::GetState() const {
	return m_currentState;
}