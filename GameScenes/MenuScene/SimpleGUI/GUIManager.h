#ifndef _BUTTONSUBJECT_H_
#define _BUTTONSUBJECT_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a gui manager. It holds the gui information. This time it holds only two
buttons for exiting and starting the game. It might be expanded to hold options and any other
button as well. Everything is controlled by the keyboard, i.e. controlSystem.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/


//forward declarations
namespace sf {
	class RenderWindow;
};
class Text;

class GUIManager {
public:
	enum ButtonState {
		START_GAME,
		EXIT
	};
public:
	GUIManager();
	~GUIManager();

	void Render(sf::RenderWindow*) const;
	void ChangeState(bool moveUp);			//if true, goes up the button state, otherwise - down
	ButtonState GetState() const;			//get the button's current state, i.e. exit or start game
private:
	void Initialize();
private:
	Text* m_Text;
	ButtonState m_currentState;
};

#endif