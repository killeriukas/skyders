#ifndef _COLLISIONDETECTIONCLASS_H_
#define _COLLISIONDETECTIONCLASS_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is responsible for the collision detection between board and the dropping piece.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//my includes
class Board;
class Piece;

//collision static class
//it doesn't change any kind of data, so it doesn't need to be a normal class
class CollisionDetectionClass {
public:
	CollisionDetectionClass();
	~CollisionDetectionClass();

	//if this collision is detected, you need to spawn new piece
	static bool isCollisionDetected(Board*, Piece*);

	//if this type of collision is detected, just move back to the previous position
	static bool isMoveCollision(Board*, Piece*);
private:
	static bool isSideCollision(Board*, Piece*);
	static bool CheckMoveLeftCollision(Board*, Piece*);
	static bool CheckMoveRightCollision(Board*, Piece*);
	static bool CheckRotationCollision(Board*, Piece*);
	static bool CheckDropCollision(Board*, Piece*);
};

#endif