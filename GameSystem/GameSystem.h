#ifndef _GAMESYSTEM_H_
#define _GAMESYSTEM_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a main game class, where the main loop of the game runs. It holds all the scenes
and other input, like input abstract input handler, etc...

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//includes, which are not allowed to fast forward
#include <string>

//fast forwards
//class AScene;
namespace sf {
	class RenderWindow;
	class Clock;
};
class SceneManager;
class InputHandler;

static const unsigned int WINDOW_HEIGHT = 500;
static const unsigned int WINDOW_WIDTH = 400;
static const int VSYNC_ENABLED = true;
static const std::string WINDOW_NAME = "Skyders by Arnold";

class GameSystem {
public:
	GameSystem();
	//static SystemClass& GetInstance() { static SystemClass SystemClass_; return SystemClass_; }
	//SceneManager& GetSceneManager() { return *m_SceneManager; }
	~GameSystem();

	void Run();
private:
	void Shutdown();
	void Initialize();
	bool Frame();
	GameSystem(const GameSystem&);
private:
	//AScene* m_GameScene;
	//AScene* m_MenuScene;
	sf::RenderWindow* m_RenderWindow;
	InputHandler* m_InputHandler;
	SceneManager* m_SceneManager;
	sf::Clock* m_clock;
};

#endif