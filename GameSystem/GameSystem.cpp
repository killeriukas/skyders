#include "GameSystem.h"
#include <sfml/Graphics.hpp>
#include "../Scenes/SceneManager.h"
#include "../GameScenes/MenuScene/MenuScene.h"
#include "../GameScenes/GameScene/GameScene.h"
#include "../InputSystem/InputHandler.h"
#include "../InputSystem/InputKeyboard.h"
#include "../InputSystem/InputMouse.h"
#include "../Util/HelpFunctions.h"

GameSystem::GameSystem() {
	m_RenderWindow = 0;
	m_SceneManager = 0;
	m_InputHandler = 0;
	m_clock = 0;
	Initialize();
}

GameSystem::GameSystem(const GameSystem& other) {

}

GameSystem::~GameSystem() {

}

void GameSystem::Initialize() {
	

	//initializes render window
	m_RenderWindow = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH,WINDOW_HEIGHT,32), WINDOW_NAME, sf::Style::Close);

	sf::Image _icon_;

	//load icon for game window
	if(!_icon_.loadFromFile("data/textures/icon.png")) {
		return;
	}

	//set icon for game window
	m_RenderWindow->setIcon(32,32, _icon_.getPixelsPtr());
	
	m_clock = new sf::Clock;

	//set vsync state
	m_RenderWindow->setVerticalSyncEnabled(VSYNC_ENABLED);

	m_InputHandler = new InputHandler(new InputKeyboard(), new InputMouse());


	//allocate memory for scene manager and set its first scene
	m_SceneManager = new SceneManager(new MenuScene(m_RenderWindow), m_InputHandler);
	m_SceneManager->AddScene(new GameScene(m_RenderWindow, (float)WINDOW_WIDTH, (float)WINDOW_HEIGHT));

}



void GameSystem::Run() {
	bool result, done;
	done = false;
	// Process events
	sf::Event event;
	//game loop
	while(!done) {

		//process all events from window
		while(m_RenderWindow->pollEvent(event)) {

			// if close button pushed, terminate game loop
			if(event.type == sf::Event::Closed) {
				done = true;
			}
		}
		//do frame processing
		result = Frame();
		if(!result) {
			done = true;
		}
	}
}

bool GameSystem::Frame() {
	//create timer and get the elapsed time between frames
	sf::Time elapTime = m_clock->restart();

	//set that time into seconds, instead of milliseconds
	//so you would not need to divide it by 1000 later on
	float frameTime = elapTime.asSeconds();

	//update current scene input
	m_SceneManager->Run(frameTime);
	
	//if exit has been activated, exits the game
	if(m_SceneManager->IsExitApplicationActivated()) {
		return false;
	}

	return true;
}

void GameSystem::Shutdown() {
	SAFE_DELETE(m_SceneManager);
	SAFE_DELETE(m_InputHandler);

	//closes the rendering window and destroys its data
	if(m_RenderWindow) {
		m_RenderWindow->close();
		delete m_RenderWindow;
		m_RenderWindow = 0;
	}

	//kill clock object
	SAFE_DELETE(m_clock);
}