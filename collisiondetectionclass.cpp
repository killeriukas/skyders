#include "collisiondetectionclass.h"
#include "GameScenes/GameScene/Board.h"
#include "GameScenes/GameScene/Piece.h"

CollisionDetectionClass::CollisionDetectionClass() {

}

CollisionDetectionClass::~CollisionDetectionClass() {

}

bool CollisionDetectionClass::isMoveCollision(Board* board, Piece* piece) {
	bool result;

	//check board side collision
	result = isSideCollision(board, piece);

	if(result) {
		return true;
	}

	//check left movement with other figures collision
	result = CheckMoveLeftCollision(board, piece);

	if(result) {
		return true;
	}

	//check right movement with other figures collision
	result = CheckMoveRightCollision(board, piece);

	if(result) {
		return true;
	}

	//check rotation collision (MIGHT NOT NEED THIS ONE)
	result = CheckRotationCollision(board, piece);

	if(result) {
		return true;
	}

	return false;
}

bool CollisionDetectionClass::isCollisionDetected(Board* board, Piece* piece) {

	bool result;

	//check drop collision, whether to add figures into the board
	result = CheckDropCollision(board, piece);

	if(result) {
		return true;
	}


	return false;
}

bool CollisionDetectionClass::isSideCollision(Board* board, Piece* piece) {

	//check left wall collision
	for(int height = 0; height < 4; ++height) {
		if((piece->GetPiece(0, height)) == 1 && (board->GetLeftSideX() > piece->GetCurrentPositionX())) {
			return true;
		}
		if((piece->GetPiece(1, height)) == 1 && (board->GetLeftSideX() > piece->GetCurrentPositionX()+board->GetCubeSize())) {
			return true;
		}
		if((piece->GetPiece(2, height)) == 1 && (board->GetLeftSideX() > piece->GetCurrentPositionX()+2*board->GetCubeSize())) {
			return true;
		}
	}

	//check right wall collision
	for(int height = 0; height < 4; ++height) {
		if((piece->GetPiece(3, height)) == 1 && (board->GetRightSideX() < piece->GetCurrentPositionX()+4*board->GetCubeSize())) {
			return true;
		}
		if((piece->GetPiece(2, height)) == 1 && (board->GetRightSideX() < piece->GetCurrentPositionX()+3*board->GetCubeSize())) {
			return true;
		}
		if((piece->GetPiece(1, height)) == 1 && (board->GetRightSideX() < piece->GetCurrentPositionX()+2*board->GetCubeSize())) {
			return true;
		}
	}

	return false;
}


bool CollisionDetectionClass::CheckMoveLeftCollision(Board* board, Piece* piece) {

	//check left collision with other figures in the board
	for(int height = 0; height < 4; ++height) {
		if((piece->GetPiece(0, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX(),piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
		if((piece->GetPiece(1, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+board->GetCubeSize(),piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
		if((piece->GetPiece(2, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+2*board->GetCubeSize(), piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
	}

	//no collision detected
	return false;
}

bool CollisionDetectionClass::CheckMoveRightCollision(Board* board, Piece* piece) {

	//check left collision with other figures in the board
	for(int height = 0; height < 4; ++height) {
		if((piece->GetPiece(3, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+3*board->GetCubeSize(), piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
		if((piece->GetPiece(2, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+2*board->GetCubeSize(), piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
		if((piece->GetPiece(1, height) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+board->GetCubeSize(), piece->GetCurrentPositionY()+height*board->GetCubeSize()) == 1)) {
			return true;
		}
	}

	//no collision detected
	return false;
}

bool CollisionDetectionClass::CheckRotationCollision(Board* board, Piece* piece) {

	//MIGHT NOT NEED THIS ONE

	return false;
}

bool CollisionDetectionClass::CheckDropCollision(Board* board, Piece* piece) {

	//check for collision with the board bottom
	for(int width = 0; width < 4; ++width) {
		if((piece->GetPiece(width, 3) == 1) && (board->GetBottomY() < piece->GetCurrentPositionY() + 4*board->GetCubeSize())) {
			return true;
		}
		if((piece->GetPiece(width, 2) == 1) && (board->GetBottomY() < piece->GetCurrentPositionY() + 3*board->GetCubeSize())) {
			return true;
		}
		if((piece->GetPiece(width, 1) == 1) && (board->GetBottomY() < piece->GetCurrentPositionY() + 2*board->GetCubeSize())) {
			return true;
		}
	}

	//check collision with board pieces from the bottom each time piece drops one unit
	for(int width = 0; width < 4; ++width) {
		if((piece->GetPiece(width, 3) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+width*board->GetCubeSize(), piece->GetCurrentPositionY()+3*board->GetCubeSize()) == 1) && piece->GetPiecePhase() == 1) {
			return true;
		}
		if((piece->GetPiece(width, 2) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+width*board->GetCubeSize(), piece->GetCurrentPositionY()+2*board->GetCubeSize()) == 1) && piece->GetPiecePhase() == 1) {
			return true;
		}
		if((piece->GetPiece(width, 1) == 1) && (board->GetBoardTileState(piece->GetCurrentPositionX()+width*board->GetCubeSize(), piece->GetCurrentPositionY()+board->GetCubeSize()) == 1) && piece->GetPiecePhase() == 1) {
			return true;
		}
	}

	//no collision detected
	return false;
}