//define windows lean include
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "GameSystem/GameSystem.h"
#include "Util/HelpFunctions.h"

//main windows method where everything starts
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow) {
	//initialize system object
	//and if initialization was OK, run it.
	GameSystem* _GameSystem = new GameSystem();
	if(_GameSystem) {
		_GameSystem->Run();
	}


	//if we finish the game loop
	//successfully shutdown object and delete it
	SAFE_DELETE(_GameSystem);
	
	return 0;
}