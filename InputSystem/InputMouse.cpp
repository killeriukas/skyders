#include "InputMouse.h"
#include "IAimSystem.h"
#include <sfml/Window.hpp>
#include "../Util/HelpFunctions.h"


InputMouse::InputMouse() {

}

InputMouse::~InputMouse() {

}

//returns pointer coordinates inside the window
void InputMouse::GetPointerLocation(int& mouseX, int& mouseY) const {
	mouseX = sf::Mouse::getPosition().x;
	mouseY = sf::Mouse::getPosition().y;
}

//return pointer deltas between frames
void InputMouse::GetPointerDeltas(float& deltaX, float& deltaY) const {

}

bool InputMouse::IsLeftMouseButtonClicked() const {
	if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		return true;
	}
	return false;
}

bool InputMouse::IsMiddleMouseButtonClicked() const {
	return false;
}

bool InputMouse::IsRightMouseButtonClicked() const {
	return false;
}

void InputMouse::Update(float) {

}