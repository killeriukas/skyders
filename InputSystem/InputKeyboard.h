#ifndef _INPUTKEYBOARD_H_
#define _INPUTKEYBOARD_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================
Input keyboard class is responsible for the keyboard input of the user. This is one of the
possible controls. It uses the SFML input layout. This class only abstracts the keyboard.
If you want to abstract joystick or anything else, create new class with IControlSystem inherited.
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

#include "IControlSystem.h"

class InputKeyboard : public IControlSystem {
public:
	InputKeyboard();
	virtual ~InputKeyboard();
	virtual bool IsLeft() const;
	virtual bool IsRight() const;
	virtual bool IsRotate() const;
	virtual bool IsDropInstant() const;
	virtual bool IsEnter() const;
	virtual void Update(float);
};

#endif