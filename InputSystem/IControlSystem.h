#ifndef _ICONTROLSYSTEM_H_
#define _ICONTROLSYSTEM_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

							CLASS FOR ABSTRACT CONTROL SYSTEM

Interface for the control system. The main buttons are expressed here, but need to be redefined
in the child classes. These buttons are only the main ones. It might need to be updated with
the new ones after a while, however, other classes won't need to be changed.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/
class IControlSystem {
public:
	
	//if any command is pressed, returns true
	//however, it depends on input class, whether this button needs to be pushed once or continually
	virtual bool IsLeft() const = 0;
	virtual bool IsRight() const = 0;
	virtual bool IsRotate() const = 0;
	virtual bool IsDropInstant() const = 0;
	virtual bool IsEnter() const = 0;
	virtual void Update(float) = 0;
};


#endif