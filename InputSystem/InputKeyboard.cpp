#include "InputKeyboard.h"
#include "IControlSystem.h"
#include <sfml/Window.hpp>
#include "../Util/HelpFunctions.h"

InputKeyboard::InputKeyboard() {

}

InputKeyboard::~InputKeyboard() {

}

bool InputKeyboard::IsRotate() const {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		return true;
	}
	return false;
}

bool InputKeyboard::IsEnter() const {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
		return true;
	}
	return false;
}

bool InputKeyboard::IsDropInstant() const {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		return true;
	}
	return false;
}

bool InputKeyboard::IsLeft() const {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		return true;
	}
	return false;
}

bool InputKeyboard::IsRight() const {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		return true;
	}
	return false;
}

void InputKeyboard::Update(float deltaMS) {

}