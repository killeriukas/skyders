#ifndef _INPUTHANDLER_H_
#define _INPUTHANDLER_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================
Input handler class is responsible for the input of the user. It takes all the information from
buttons and a pointer and pass it to the programme. It's like a bridge between abstract input
devices. All the control and aim classes should be inherited from IControlSystem and IAimSystem
interfaces and passed to this class through the constructor.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//forward declarations
class IControlSystem;
class IAimSystem;

class InputHandler {
public:
	InputHandler(IControlSystem* const, IAimSystem* const);
	~InputHandler();

	//checks for button presses
	void GetPointerLocation(int&, int&) const;
	bool IsLeftMouseButtonClicked() const;
	bool IsEnter() const;
	bool IsLeft() const;
	bool IsRight() const;
	bool IsRotate() const;
	bool IsDropInstant() const;
	void Update(float);
private:
	IControlSystem * m_ControlDevice;
	IAimSystem * m_AimDevice;
};

#endif