#ifndef _INPUTMOUSE_H_
#define _INPUTMOUSE_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================
Input mouse class is responsible for the mouse input of the user. This is one of the
possible controls. It uses SFML input layout. This class only abstracts the mouse.
If you want to abstract sticks or anything else, create new class with IAimSystem inherited.
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

#include "IAimSystem.h"

class InputMouse : public IAimSystem {
public:
	InputMouse();
	virtual ~InputMouse();

	//returns pointer coordinates inside the window
	virtual void GetPointerLocation(int&, int&) const;

	//return pointer deltas between frames
	virtual void GetPointerDeltas(float&, float&) const;

	//if mouse button has been clicked, it returns true. otherwise - false;
	virtual bool IsLeftMouseButtonClicked() const;
	virtual bool IsMiddleMouseButtonClicked() const;
	virtual bool IsRightMouseButtonClicked() const;

	virtual void Update(float);
};

#endif