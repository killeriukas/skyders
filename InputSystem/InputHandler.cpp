#include "InputHandler.h"
#include "IControlSystem.h"
#include "IAimSystem.h"
#include "../Util/HelpFunctions.h"

InputHandler::InputHandler(IControlSystem* const iControlSystem, IAimSystem* const iAimSystem) : m_ControlDevice(iControlSystem), m_AimDevice(iAimSystem) {

}

InputHandler::~InputHandler() {
	SAFE_DELETE(m_ControlDevice);
	SAFE_DELETE(m_AimDevice);
}

void InputHandler::GetPointerLocation(int& x, int& y) const {
	MYASSERT_VOID(m_AimDevice);
	m_AimDevice->GetPointerLocation(x, y);
}

bool InputHandler::IsRotate() const {
	MYASSERT_BOOL(m_ControlDevice);
	return m_ControlDevice->IsRotate();
}

bool InputHandler::IsDropInstant() const {
	MYASSERT_BOOL(m_ControlDevice);
	return m_ControlDevice->IsDropInstant();
}

bool InputHandler::IsLeft() const {
	MYASSERT_BOOL(m_ControlDevice);
	return m_ControlDevice->IsLeft();
}

bool InputHandler::IsEnter() const {
	MYASSERT_BOOL(m_ControlDevice);
	return m_ControlDevice->IsEnter();
}

bool InputHandler::IsRight() const {
	MYASSERT_BOOL(m_ControlDevice);
	return m_ControlDevice->IsRight();
}

bool InputHandler::IsLeftMouseButtonClicked() const {
	MYASSERT_BOOL(m_AimDevice);
	return m_AimDevice->IsLeftMouseButtonClicked();
}

void InputHandler::Update(float deltaMS) {
	MYASSERT_VOID(m_ControlDevice);
	m_ControlDevice->Update(deltaMS);
	MYASSERT_VOID(m_AimDevice);
	m_AimDevice->Update(deltaMS);
}