#ifndef _IAIMSYSTEM_H_
#define _IAIMSYSTEM_H_
/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

							CLASS FOR ABSTRACT AIM SYSTEM

Interface for the aim system. The main pointers are expressed here, but need to be redefined
in the child classes. These pointers are only the main ones. It might need to be updated with
the new ones after a while, however, other classes won't need to be changed.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/
class IAimSystem {
public:

	//returns pointer coordinates inside the window
	virtual void GetPointerLocation(int&, int&) const = 0;

	//return pointer deltas between frames
	virtual void GetPointerDeltas(float&, float&) const = 0;

	//if mouse button has been clicked, it returns true. otherwise - false;
	virtual bool IsLeftMouseButtonClicked() const = 0;
	virtual bool IsMiddleMouseButtonClicked() const = 0;
	virtual bool IsRightMouseButtonClicked() const = 0;

	virtual void Update(float) = 0;
};

#endif