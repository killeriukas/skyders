#include "Text.h"
#include <sfml/Graphics.hpp>
#include "../Util/HelpFunctions.h"

Text::Text() {
	m_Text = 0;
	m_Font = 0;
}

bool Text::Initialize(float x, float y) {

	bool result;

	m_Font = new sf::Font();

	//initialize the font
	result = m_Font->loadFromFile("data/font/comic.ttf");

	if(!result) {
		return false;
	}
	
	m_Text = new sf::Text();

	//set text object
	m_Text->setFont(*m_Font);
	m_Text->setCharacterSize(20);
	m_Text->setPosition(sf::Vector2f(x, y));
	m_Text->setColor(sf::Color(255,255,255));

	return true;
}

Text::~Text() {
	SAFE_DELETE(m_Font);
	SAFE_DELETE(m_Text);
}

void Text::SetTextColor(int r, int g, int b) {
	m_Text->setColor(sf::Color(r,g,b));
}

void Text::SetSentence(const std::string& sentence) {
	//set new sentence
	m_Text->setString(sentence);
}

void Text::Render(sf::RenderWindow* const renderWindow) {
	//render text
	renderWindow->draw(*m_Text);
}