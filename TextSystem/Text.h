#ifndef _TEXT_H_
#define _TEXT_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================
Text class is responsible for loading the font and drawing it to the screen.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//impossible to fast forward
#include <string>

//fast declarations
namespace sf {
	class RenderWindow;
	class Text;
	class Font;
};


class Text {
public:
	Text();
	~Text();
	bool Initialize(float, float);
	void Render(sf::RenderWindow* const);
	void SetSentence(const std::string&);
	void SetTextColor(int,int,int);
private:
	std::string ms_Score;
	sf::Text* m_Text;
	sf::Font* m_Font;
};

#endif