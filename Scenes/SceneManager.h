#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a scene manager, which allows you to create main menu, game-play, end game scenes
and much more.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//impossible to fast forward templates
#include <string>
#include <map>

//forward declarations
class AScene;
class InputHandler;


//THINK ABOUT PAUSE AND RESUME FUNCTIONS MORE
//IT IS NOT GOOD TO INSTANTLY PAUSE AND ReSUME THE GAME, OR IS IT GOOD? 
//NEED TO THINK ABOUT THAT MORE

class SceneManager {
public:
	SceneManager(AScene* const, const InputHandler* const);
	~SceneManager();

	void Run(float);
	void AddScene(AScene*);
	void ChangeScene(std::string);
	void ExitApplication();
	bool IsExitApplicationActivated() const;
private:
	void UpdateInput(float);
	void UpdateGameLogic(float);
	void Render() const;
	void CheckWaitingSceneStatus();
	void PauseScene(std::string);
	void ResumeScene();
	void DeleteMap();
private:
	std::map<std::string,AScene*> m_SceneMap;
	//std::map<std::string,IEvent*> m_EventMap;
	std::string m_activeScene;
	std::string m_pauseScene;
	std::string m_waitingScene;
	bool m_exitActivated;
	const InputHandler* m_InputHandler;
};

#endif