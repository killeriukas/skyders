#include "AScene.h"
#include "SceneManager.h"

AScene::AScene(std::string sceneName) : m_sceneName(sceneName) {
	m_InputHandler = 0;
}

AScene::~AScene() {
	m_SceneManagerPtr = 0;
	m_InputHandler = 0;
}

void AScene::ChangeScene(std::string nextScene) {
	m_SceneManagerPtr->ChangeScene(nextScene);
}

void AScene::SetInputHandler(const InputHandler* const inputHandler) {
	m_InputHandler = inputHandler;
}

void AScene::ExitApplication() {
	m_SceneManagerPtr->ExitApplication();
}

std::string AScene::GetSceneName() const {
	return m_sceneName;
}

void AScene::SetSceneManagerPointer(SceneManager* const sceneManager) {
	m_SceneManagerPtr = sceneManager;
}