#ifndef _ASCENE_H_
#define _ASCENE_H_

/*
===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  START												  |
===============================================================================================

This class is a abstract class for the scene classes. Inherit this class if you want to create
scenes such as main menu, game play, pause, game over scenes.

===============================================================================================
|																							  |
|									CLASS EXPLANATION										  |
|										  END												  |
===============================================================================================
*/

//fast forward declaration
class SceneManager;
class InputHandler;

//impossible to fast forward declare
#include <string>

class AScene {
public:
	AScene(std::string sceneName);
	virtual ~AScene();

	//abstract play functions
	virtual void Start() = 0;			//all start functions goes into this method
	virtual void Stop() = 0;			//all the shutdown functions goes into this method
	virtual void Pause() = 0;
	virtual void Resume() = 0;

	//abstract game functions
	virtual void UpdateInput(float) = 0;
	virtual void UpdateGameLogic(float) = 0;
	virtual void Render() = 0;

	std::string GetSceneName() const;
	void SetSceneManagerPointer(SceneManager* const);
	void SetInputHandler(const InputHandler* const);
protected:
	void ChangeScene(std::string);
	void ExitApplication();
protected:
	const InputHandler* m_InputHandler;
private:
	std::string m_sceneName;
	SceneManager* m_SceneManagerPtr;
};

#endif