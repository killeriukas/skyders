#include "SceneManager.h"
#include "AScene.h"
#include "../InputSystem/InputHandler.h"
#include "../Util/HelpFunctions.h"

SceneManager::SceneManager(AScene* const firstScene, const InputHandler* const inputHandler) {
	m_pauseScene = "";
	m_waitingScene = "";
	m_InputHandler = inputHandler;
	AddScene(firstScene);
	m_activeScene = firstScene->GetSceneName();
	firstScene->Start();
	m_exitActivated = false;
}

bool SceneManager::IsExitApplicationActivated() const {
	return m_exitActivated;
}


void SceneManager::Render() const {
	m_SceneMap.at(m_activeScene)->Render();
}

void SceneManager::Run(float deltaTimeMS) {
	CheckWaitingSceneStatus();
	UpdateInput(deltaTimeMS);
	UpdateGameLogic(deltaTimeMS);
	Render();
}


void SceneManager::UpdateInput(float deltaTimeMS) {
	m_SceneMap.at(m_activeScene)->UpdateInput(deltaTimeMS);
}

void SceneManager::CheckWaitingSceneStatus() {
	//checks whether there is a pending scene
	if(strcmp(m_waitingScene.c_str(), "") != 0) {

		//if we got a pending scene, we shutdown this scene
		//and change active scene to the pending one
		//change pending screen into NULL
		m_SceneMap.at(m_activeScene)->Stop();
		m_activeScene = m_waitingScene;
		m_waitingScene = "";
	}
}

void SceneManager::UpdateGameLogic(float deltaTimeMS) {
	m_SceneMap.at(m_activeScene)->UpdateGameLogic(deltaTimeMS);
}

void SceneManager::AddScene(AScene* sceneObject) {
	//before adding a scene into the array, SceneManager sets this scene inputhandler and pointer of manager
	sceneObject->SetSceneManagerPointer(this);
	sceneObject->SetInputHandler(m_InputHandler);
	m_SceneMap.insert(std::pair<std::string,AScene*>(sceneObject->GetSceneName(),sceneObject));
}

void SceneManager::ChangeScene(std::string nextScene) {
	//starts following scene
	m_SceneMap.at(nextScene)->Start();

	//sets it into pending position
	m_waitingScene = nextScene;
}

void SceneManager::PauseScene(std::string pauseScene) {
	//pause the active one
	m_SceneMap.at(m_activeScene)->Pause();

	//set pause screen into active one
	m_pauseScene = m_activeScene;

	//set new active screen
	m_activeScene = pauseScene;

	//start pause screen
	m_SceneMap.at(m_activeScene)->Start();
}

void SceneManager::ResumeScene() {
	//stop pause screen
	m_SceneMap.at(m_activeScene)->Stop();

	//set active screen to previous one
	m_activeScene = m_pauseScene;

	//set previous screen into NULL
	m_pauseScene = "";

	//resume the rendering of previous active scene
	m_SceneMap.at(m_activeScene)->Resume();

}

SceneManager::~SceneManager() {
	//clear all the container
	DeleteMap();
}

void SceneManager::ExitApplication() {
	m_exitActivated = true;
}

void SceneManager::DeleteMap() {
	std::map<std::string, AScene*>::iterator iter;
	for(iter = m_SceneMap.begin(); iter != m_SceneMap.end(); ++iter) {
		iter->second->Stop();
		delete iter->second;
		iter->second = 0;
	}
	m_SceneMap.clear();
}