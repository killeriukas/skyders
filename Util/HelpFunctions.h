#ifndef _HELPFUNCTIONS_H_
#define _HELPFUNCTIONS_H_
/*
===============================================================================================
|																							  |
|								CONST VARIABLE EXPLANATIONS									  |
|										   STARTS											  |
===============================================================================================

If you mark a method const it means that this will be of type const MyClass* instead of MyClass*,
which in its turn means that you cannot change nonstatic data members that are not declared
mutable, nor can you call any non-const methods.

Now for the return value.

1 . int * const func () const

The function is constant, and the returned pointer is constant but the 'junk inside'
can be modified. However I see no point in returning a const pointer because the ultimate
function call will be an rvalue, and rvalues of non-class type cannot be const, meaning that
const will be ignored anyway

2 . const int* func () const

This is a useful thing. The "junk inside" cannot be modified

3 . const int * const func() const

semantically almost the same as 2, due to reasons in 1.

ANOTHER EXPLANATION, IN CASE THE FIRST ONE WAS NOT ENOUGH

1) Just making the pointed value const

	void someFunc1(const int * arg) {
		int i = 9;
		*arg = i; // <- compiler error as pointed value is const
	}

2) Just making the pointer const

	void someFunc1(int * const arg) {
		int i = 9;
		arg = &i; // <- compiler error as pointer is const
	}

3) Right way to use const if variables involved can be const:

	void someFunc1(const int * const arg) {
		int i = 9;
		*arg = i; // <- compiler error as pointed value is const
		arg = &i; // <- compiler error as pointer is const
	}

===============================================================================================
|																							  |
|								CONST VARIABLE EXPLANATIONS									  |
|											END												  |
===============================================================================================
*/

//include of the debug mode
#include "DebugAssert.h"

//include DirectX math library if it exists
//if it doesn't create equivalent functions for float variables
#ifdef __D3DX10_H__
#include <D3DX10math.h>

__forceinline float DistanceBetweenTwo2DPointsCheap(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2) {
	//we don't care what is the height difference between points, because we divide only on 2D plane, which is (x,z)
	return (v1.x - v2.x)*(v1.x - v2.x) + (v1.z - v2.z)*(v1.z - v2.z);
}
__forceinline float DistanceBetweenTwo3DPointsCheap(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2) {
	return (v1.x - v2.x)*(v1.x - v2.x) + (v1.y - v2.y)*(v1.y - v2.y) + (v1.z - v2.z)*(v1.z - v2.z);
}
#else
__forceinline float DistanceBetweenTwo2DPointsCheap(float x1, float z1, float x2, float z2) {
	//we don't care what is the height difference between points, because we divide only on 2D plane, which is (x,z)
	return (x1 - x2)*(x1 - x2) + (z1 - z2)*(z1 - z2);
}
__forceinline float DistanceBetweenTwo3DPointsCheap(float x1, float y1, float z1, float x2, float y2, float z2) {
	return (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) +(z1 - z2)*(z1 - z2);
}
#endif

//shortcuts for deletion
#define SAFE_DELETE(x) if(x) { delete x; x = 0; }
#define SAFE_ARRAY_DELETE(x) if(x) { delete[] x; x = 0; }
#define SAFE_CLASS_DELETE(x) if(x) { x->Shutdown(); delete x; x = 0; }
#define SAFE_D3DXBUFFER_DELETE(x) if(x) { x->Release(); x = 0; }
#define SAFE_CONST_DELETE(x) if(x) { delete x; }

//shortcuts to MIN and MAX macros
#define MIN(x,y) ((x > y) ? y : x)
#define MAX(x,y) MIN(y,x)

//type definitions, which might be used later on
typedef unsigned int Uint;
typedef unsigned long Ulong;

#endif //_HELPFUNCTIONS_H_