#ifndef _FILECONSOLE_H_
#define _FILECONSOLE_H_

#include "IConsole.h"

class FileConsole : public IConsole {
public:
	FileConsole();
	virtual ~FileConsole();
	virtual void Create(const char*);
	virtual void Write(const char*, ...);
	virtual void Close();
private:
	FILE* m_file;
};
#endif