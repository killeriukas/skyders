#ifndef _DEBUGASSERT_H_
#define _DEBUGASSERT_H_

//if this is not commented, that means we are in debug mode, and all the asserts and other stuff should be included as well
//#define DEBUGMODE

//for debug purposes only and not gonna implemented into the release build
#ifdef DEBUGMODE
#include <assert.h>
#define MYASSERT_VOID(x) assert(x)
#define MYASSERT_BOOL(x) assert(x)
#define MYASSERT_INT(x) assert(x) 
#define MYASSERT_INT_ERROR(x) assert(x)
#define MYASSERT_HRESULT_VOID(x) assert(!x)
#define MYASSERT_HRESULT_BOOL(x) assert(!x)
#else
#define MYASSERT_VOID(x) { if(!x) { return; }}
#define MYASSERT_BOOL(x) { if(!x) { return false; }}
#define MYASSERT_INT(x) { if(!x) { return 0; }}
#define MYASSERT_INT_ERROR(x) { if(!x) { return -1; }}
#define MYASSERT_HRESULT_VOID(x) MYASSERT_VOID(!x)
#define MYASSERT_HRESULT_BOOL(x) MYASSERT_BOOL(!x)
#endif

#endif