#ifndef _ICONSOLE_H_
#define _ICONSOLE_H_

#include <iostream>
#include <fstream>

#define DEBUG_ON

class IConsole {
public:
	//virtual ~IConsole() {} = 0;
	virtual void Create(const char*) = 0;
	virtual void Write(const char*, ...) = 0;
	virtual void Close() = 0;
};

#endif