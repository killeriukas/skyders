#ifndef _SINGLETON_LOG_H_
#define _SINGLETON_LOG_H_

#include "IConsole.h"
//#include "FileConsole.h"

class SLog {
public:

	SLog& GetInstance() { return m_instance; };
	void Write(const char*, ...);

private:
	SLog();
	SLog(const SLog&);
	~SLog();

private:
	//FileConsole m_FileConsole;
	SLog m_instance;
};
#endif